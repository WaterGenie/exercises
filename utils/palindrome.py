def reverse(number: int) -> int:
    n = number
    reverse_number = 0
    while n > 0:
        digit = n % 10
        reverse_number = (10 * reverse_number) + digit
        n //= 10
    return reverse_number


def is_palindrome(number: int) -> bool:
    if number == 0:
        return True
    if number < 0:
        number = -number

    return number == reverse(number)
