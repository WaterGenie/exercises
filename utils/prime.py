from math import ceil
from math import sqrt


def is_prime(n: int):
    if n < 2:
        return False
    if n < 4:
        return True
    if n % 2 == 0:
        return False

    i = 3
    threshold = ceil(sqrt(n))
    while i <= threshold:
        if n % i == 0:
            return False
        i += 2
    return True


def primes_up_to(n: int):
    pass
