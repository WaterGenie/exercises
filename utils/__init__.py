from .fibonacci import *
from .multiples import *
from .palindrome import *
from .prime import *
