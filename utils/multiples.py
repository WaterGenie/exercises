def is_multiple_of_or(target: int, multiples: list[int]):
    for multiple in multiples:
        if target % multiple == 0:
            return True
    return False
