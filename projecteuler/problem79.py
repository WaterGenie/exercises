# https://projecteuler.net/problem=79
# Given a passcode of length 3 or greater, 3 random digits will be picked and
# queried for each access attempt.
# The 3 digits preserve their passcode order from left to right.
# Given a list of queries, determine the shortest possible passcode.

queries = [
    319, 680, 180, 690, 129, 620, 762, 689, 762, 318, 368, 710, 720, 710, 629,
    168, 160, 689, 716, 731, 736, 729, 316, 729, 729, 710, 769, 290, 719, 680,
    318, 389, 162, 289, 162, 718, 729, 319, 790, 680, 890, 362, 319, 760, 316,
    729, 380, 319, 728, 716
]

def shortest_passcode_length(queries: list[int]) -> int:
    pass



if __name__ == '__main__':
    #
    print(shortest_passcode_length(queries))
