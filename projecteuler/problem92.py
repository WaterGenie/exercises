# https://projecteuler.net/problem=92
# A number chain is created by continuously adding the square of the digits in
# a number to form a new number until it has been seen before.
# Every starting number will eventually arrive at 1 or 89.
# How many starting numbers below ten million will arrive at 89?

def sum_of_squares_of_digits(num: int) -> int:
    n = num
    sum_so_far = 0
    
    while n > 0:
        digit = n % 10
        squared_digit = digit * digit
        sum_so_far += squared_digit
        n //= 10

    return sum_so_far


def chain(starting_num: int, memory: dict[int, int]) -> int:
    if len(memory) == 0:
        memory[1] = 1
    
    if starting_num not in memory:
        next_num = sum_of_squares_of_digits(starting_num)
        if next_num == 1:
            memory[next_num] = 1
        elif next_num == 89:
            memory[next_num] = 89
        else:
            memory[next_num] = chain(next_num, memory)
        memory[starting_num] = memory[next_num]

    return memory[starting_num]


def numbers_end_in_89_under(threshold: int) -> int:
    count_so_far = 0
    memory = {1: 1}

    for n in range(1, threshold):
        ends = chain(n, memory)
        if ends == 89:
            count_so_far += 1

    return count_so_far


if __name__ == '__main__':
    # 8_581_146
    print(numbers_end_in_89_under(10_000_000))
