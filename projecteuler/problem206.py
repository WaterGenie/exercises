# https://projecteuler.net/problem=206
# Find the unique positive integer whose square has the form 1_2_3_4_5_6_7_8_9_0,
# where each “_” is a single digit.

from math import ceil
from math import floor
from math import sqrt


MAX_POSSIBLE = 1_929_394_959_697_989_990
MIN_POSSIBLE = 1_020_304_050_607_080_900


def is_concealed(num: int) -> bool:
    if num > MAX_POSSIBLE or num < MIN_POSSIBLE:
        return False
    
    digit_format_at = [0, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    n = num
    i = 0
    while n > 0:
        digit = n % 10
        if digit != digit_format_at[i]:
            return False
        n //= 100
        i += 1
    return True


def concealed_square():
    # Handle boundary cases
    lower_bound = sqrt(MIN_POSSIBLE)
    upper_bound = sqrt(MAX_POSSIBLE)
    if lower_bound == ceil(lower_bound):
        return lower_bound
    if upper_bound == floor(upper_bound):
        return upper_bound
    lower_bound = ceil(lower_bound)
    upper_bound = floor(upper_bound)

    for i in range(lower_bound, upper_bound):
        candidate = i * i
        if is_concealed(candidate):
            return i
    return None


if __name__ == '__main__':
    # 1_389_019_170
    print(concealed_square())
