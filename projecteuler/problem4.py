# https://projecteuler.net/problem=4
# Find the largest palindrome made from the product of two 3-digit numbers.

from utils import is_palindrome


def largest_palindrome_product(digits: int):
    largest_so_far = -1

    # can optimise a bit when we get to the point where all other products
    # will be lte largest_so_far
    largest_factor = 10**digits - 1
    for factor1 in range(largest_factor, 0, -1):
        for factor2 in range(factor1, 0, -1):
            product = factor1 * factor2
            if product <= largest_so_far:
                break

            if is_palindrome(product):
                largest_so_far = product
                break
        
    if largest_so_far == -1:
        return None
    else:
        return largest_so_far


if __name__ == '__main__':
    # 9_009
    print(largest_palindrome_product(2))
    # 906_609
    print(largest_palindrome_product(3))
