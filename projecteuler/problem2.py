# https://projecteuler.net/problem=2
# Find the sum of even fibonacci numbers below 4,000,000
# Start the fibonacci sequence with 1, 2


def brute_force(upper_bound: int):
    n2 = 1
    n1 = 2
    n = n2 + n1

    total = 2
    while n < upper_bound:
        if n % 2 == 0:
            total += n
        n2 = n1
        n1 = n
        n = n2 + n1
    
    return total


def skip(upper_bound: int):
    # Since even fibonacci is separated by 2 odd ones starting from the 2nd
    # and the 5th, we can go up the sequence by 3 instead.
    # n:    1 2 3 4 5  6  7  8 ..
    # f(n): 1 2 3 5 8 13 21 34 ..
    #       o e o o e  o  o  e ..
    # So we want f(n) in terms of f(n-3) and f(n-6)

    # f(n) =          f(n-1)     +      f(n-2)
    #      =     f(n-2) + f(n-3) + f(n-3)  + f(n-4)
    #      = f(n-3) + f(n-4) + 2f(n-3) + f(n-5) + f(n-6)
    #      = 4f(n-3) + f(n-6)
    n2 = 2
    n1 = 8
    n = (4*n1) + n2

    total = n1 + n2
    while n < upper_bound:
        total += n
        n2 = n1
        n1 = n
        n = (4*n1) + n2
    
    return total


if __name__ == '__main__':
    # 4,613,732
    print(brute_force(4_000_000))
    print(skip(4_000_000))
