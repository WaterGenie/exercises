# https://projecteuler.net/problem=18
# Find the maximum total from top to bottom of the triangle below:

demo_triangle = [
    [3],
    [7, 4],
    [2, 4, 6],
    [8, 5, 9, 3]
]


triangle = [
    [75],
    [95, 64],
    [17, 47, 82],
    [18, 35, 87, 10],
    [20, 4, 82, 47, 65],
    [19, 1, 23, 75, 3, 34],
    [88, 2, 77, 73, 7, 63, 67],
    [99, 65, 4, 28, 6, 16, 70, 92],
    [41, 41, 26, 56, 83, 40, 80, 70, 33],
    [41, 48, 72, 33, 47, 32, 37, 16, 94, 29],
    [53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14],
    [70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57],
    [91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48],
    [63, 66, 4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31],
    [4, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 4, 23]
]


def maximum_path_sum(triangle: list[list[int]]) -> int:
    running_sum = []
    for i, row in enumerate(triangle):
        row_paths = []

        for j, num in enumerate(row):
            if i-1 < 0:
                row_paths.append(num)
            else:
                left_parent = -float('inf')
                right_parent = -float('inf')
                if j-1 >= 0:
                    left_parent = running_sum[i-1][j-1]
                if j < i:
                    right_parent = running_sum[i-1][j]
                row_paths.append(num + max(left_parent, right_parent))

        running_sum.append(row_paths)

    maximum_paths = running_sum[-1]
    return max(maximum_paths)


if __name__ == '__main__':
    # 23
    print(maximum_path_sum(demo_triangle))
    # 1_074
    print(maximum_path_sum(triangle))
