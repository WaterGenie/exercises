# https://projecteuler.net/problem=14
# Find a number less than 1 million with the longest Collatz sequence.
# The sequence is defined as:
#     n -> n/2 if n is even
#     n -> 3n + 1 if n is odd
#     for all positive integer n
#     sequence ends if n is 1
# Note that starting with k, the numbers in the sequence can be greater than k.


def collatz_length(num: int, memory: dict[int, int]):
    if len(memory) == 0:
        memory[1] = 1

    if num not in memory:
        next_num = None
        if num % 2 == 0:
            next_num = num // 2
        else:
            next_num = (3 * num) + 1

        length = collatz_length(next_num, memory)
        memory[num] = length + 1

    return memory[num]


def start_num_with_longest_collatz_under(threshold: int):
    memory = {1: 1}
    longest_so_far = -float('inf')
    start_num_with_longest_so_far = None

    for n in range(1, threshold):
        length = collatz_length(n, memory)
        if length > longest_so_far:
            longest_so_far = length
            start_num_with_longest_so_far = n

    return start_num_with_longest_so_far


if __name__ == '__main__':
    # 837_799
    print(start_num_with_longest_collatz_under(1_000_000))
