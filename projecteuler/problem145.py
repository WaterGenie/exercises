# https://projecteuler.net/problem=145
# A reversible number n is a positive integer such that the sum
# n + reverse(n) consists entirely of odd digits. Leading zeroes are not
# allowed in either n or reverse(n).
# How many reversible numbers are there below one billion?

def reverse(n: int) -> int:
    i = n
    reverse_n = 0
    while i > 0:
        digit = i % 10
        reverse_n = (10 * reverse_n) + digit
        i //= 10
    return reverse_n


def is_reversible(n: int) -> bool:
    if n % 10 == 0:
        return False

    reverse_n = reverse(n)
    sum_n_reverse_n = n + reverse_n

    i = sum_n_reverse_n
    while i > 0:
        digit = i % 10
        if digit % 2 == 0:
            return False
        i //= 10
    return True


def num_reversibles_below(threshold: int) -> int:
    count_so_far = 0

    for i in range(1, threshold):
        if is_reversible(i):
            count_so_far += 1

    return count_so_far


if __name__ == '__main__':
    # 120
    print(num_reversibles_below(1_000))
    # 608_720
    print(num_reversibles_below(1_000_000_000))
