# https://projecteuler.net/problem=3
# Find the largest prime factor of 600_851_475_143

from math import ceil
from utils import is_prime


def largest_prime_factor(n: int):
    i = ceil(n / 2)
    min_threshold = 2
    if i % 2 == 0:
        i -= 1
    while i >= min_threshold:
        if n % i == 0 and is_prime(i):
            return i
        i -= 2
    return -1


if __name__ == '__main__':
    # 
    print(largest_prime_factor(600_851_475_143))
