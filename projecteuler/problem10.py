# https://projecteuler.net/problem=10
# Find the sum of all the primes below two million.
# Consider 2 as the first prime.

from utils import is_prime


def sum_of_primes_below(threshold: int):
    sum_so_far = 0
    if threshold > 2:
        sum_so_far = 2

    for n in range(3, threshold, 2):
        if is_prime(n):
            sum_so_far += n

    return sum_so_far


if __name__ == '__main__':
    # 17
    print(sum_of_primes_below(10))
    # 142_913_828_922
    print(sum_of_primes_below(2_000_000))
