# https://projecteuler.net/problem=7
# Find the 10,001st prime number.
# Consider 2 as the first prime.

from utils import is_prime


def nth_prime(n: int):
    if n == 1:
        return 2
    
    # n > 1
    candidate = 1
    count_so_far = 1

    while count_so_far < n:
        candidate += 2
        if is_prime(candidate):
            count_so_far += 1

    return candidate


if __name__ == '__main__':
    # 13
    print(nth_prime(6))

    # 104_743
    print(nth_prime(10_001))
