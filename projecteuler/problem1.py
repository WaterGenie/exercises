# https://projecteuler.net/problem=1
# Find the sum of all multiples of 3 or 5 below 1,000


from utils import is_multiple_of_or


def sum_of_multiples_below(multiples: list[int], upper_bound: int):
    total = 0
    for i in range(1, upper_bound):
        if is_multiple_of_or(i, multiples):
            total += i
    return total


if __name__ == '__main__':
    # 233,168
    print(sum_of_multiples_below([3, 5], 1000))
