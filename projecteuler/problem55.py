# https://projecteuler.net/problem=55
# How many Lychrel numbers are there below 10,000?
# A Lychrel number is an integer that never forms a palindrome when iteratively
# added to its reverse.
# Assume that a Lychrel number (below 10,000) will reach a palindrome within
# 50 iterations.

from utils import is_palindrome
from utils import reverse

def is_lychrel(num: int) -> bool:
    max_iter = 50
    iter_so_far = 0
    n = num
    while iter_so_far < max_iter:
        iter_so_far += 1
        n = n + reverse(n)
        if is_palindrome(n):
            return False

    return True


def num_lychrel_below(threshold: int) -> int:
    count_so_far = 0

    for num in range(1, threshold):
        if is_lychrel(num):
            count_so_far += 1

    return count_so_far


if __name__ == '__main__':
    # 249
    print(num_lychrel_below(10_000))
