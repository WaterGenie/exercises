from setuptools import setup
from setuptools import find_packages


# Run with python setup.py install
setup(
    name='exercises',
    packages=find_packages()
)
