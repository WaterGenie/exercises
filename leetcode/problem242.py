# https://leetcode.com/problems/valid-anagram/
# Given two strings s and t, return true if t is an anagram of s, and false
# otherwise.

# An Anagram is a word or phrase formed by rearranging the letters of a
# different word or phrase, typically using all the original letters exactly
# once.

class Solution:
    def getCharFrequency(self, s: str) -> dict[str, int]:
        frequency = {}
        for char in s:
            if char in frequency:
                frequency[char] += 1
            else:
                frequency[char] = 1
        return frequency

    def isAnagram(self, s: str, t: str) -> bool:
        char_frequency_1 = self.getCharFrequency(s)
        char_frequency_2 = self.getCharFrequency(t)

        if len(char_frequency_1.keys()) != len(char_frequency_2.keys()):
            return False
        
        for char, frequency_1 in char_frequency_1.items():
            if char not in char_frequency_2:
                return False
            
            frequency_2 = char_frequency_2[char]
            if frequency_1 != frequency_2:
                return False
        
        return True
