# https://leetcode.com/problems/split-a-string-in-balanced-strings/
# Balanced strings are those that have an equal quantity of 'L' and 'R'
# characters.

# Given a balanced string s, split it into some number of substrings such that:

# Each substring is balanced.

# Return the maximum number of balanced strings you can obtain.

class Solution:
    def balancedStringSplit(self, s: str) -> int:
        # The maximum number of balanced strings can be obtained by splitting
        # at every possible opportunity when looping through the string.
        l_count = 0
        r_count = 0
        num_balanced_string = 0

        for char in s:
            if char == 'L':
                l_count += 1
            if char == 'R':
                r_count += 1

            if l_count == r_count:
                num_balanced_string += 1
                l_count = 0
                r_count = 0

        return num_balanced_string
