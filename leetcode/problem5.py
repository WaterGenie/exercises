# https://leetcode.com/problems/longest-palindromic-substring/
# Given a string s, return the longest palindromic substring in s.

class Solution:
    def longestPalindrome(self, s: str) -> str:
        longest_palindrome = ""
        max_length = 0
        for i in range(len(s)):
            odd_palindrome = self.longestPalindromeFromPosition(s, i, i)
            even_palindrome = ""
            if i + 1 < len(s):
                even_palindrome = self.longestPalindromeFromPosition(s, i, i+1)
            longer_palindrome = odd_palindrome
            if len(odd_palindrome) < len(even_palindrome):
                longer_palindrome = even_palindrome
            
            if len(longer_palindrome) > max_length:
                max_length = len(longer_palindrome)
                longest_palindrome = longer_palindrome
            
        return longest_palindrome

    def longestPalindromeFromPosition(self, s: str, left: int, right: int) -> int:
        left_head = left
        right_head = right
        while (0 <= left_head) and (right_head < len(s)) and s[left_head] == s[right_head]:
            left_head -= 1
            right_head += 1

        return s[(left_head + 1):right_head]
