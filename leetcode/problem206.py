# https://leetcode.com/problems/reverse-linked-list/
# Given the head of a singly linked list, reverse the list, and return the
# reversed list.

# Follow up: A linked list can be reversed either iteratively or recursively.
# Could you implement both?

from typing import Optional


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        return self.reverseListIterative(head)

    def reverseListRecursive(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if head is None or head.next is None:
            return head

        # Assume the recursion returns the head (originally the tail)
        remaining_nodes = head.next
        reversed_head = self.reverseListRecursive(remaining_nodes)
        # Then we swap around the pivot
        head.next.next = head
        head.next = None  # will be redirected recursively if this is not the last element

        return reversed_head

    def reverseListIterative(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if head is None or head.next is None:
            return head

        reversed_head = None
        current = head
        # Process:
        #   1. Store the remaining nodes
        #   2. Redirect the current head to point to the reversed head so far
        #   3. Update pointers for next iteration
        while current:
            remaining = current.next
            current.next = reversed_head
            reversed_head = current
            current = remaining

        return reversed_head
