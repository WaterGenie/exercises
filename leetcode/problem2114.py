# https://leetcode.com/problems/maximum-number-of-words-found-in-sentences/
# A sentence is a list of words that are separated by a single space with no
# leading or trailing spaces.
# You are given an array of strings sentences, where each sentences[i]
# represents a single sentence.
# Return the maximum number of words that appear in a single sentence.

from typing import List


class Solution:
    def mostWordsFound(self, sentences: List[str]) -> int:
        max_num_words = 0
        for sentence in sentences:
            num_words = len(sentence.split(" "))
            if num_words > max_num_words:
                max_num_words = num_words
        return max_num_words
