# https://leetcode.com/problems/shuffle-string/
# You are given a string s and an integer array indices of the same length.
# The string s will be shuffled such that the character at the ith position
# moves to indices[i] in the shuffled string.

# Return the shuffled string.

from typing import List


class Solution:
    def restoreString(self, s: str, indices: List[int]) -> str:
        shuffled_string = [None] * len(indices)

        for i, char in enumerate(s):
            target_index = indices[i]
            shuffled_string[target_index] = char

        shuffled_string = "".join(shuffled_string)
        return shuffled_string
