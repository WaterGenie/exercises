# https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/
# Suppose an array of length n sorted in ascending order is rotated between 1
# and n times. For example, the array nums = [0,1,2,4,5,6,7] might become:

# [4,5,6,7,0,1,2] if it was rotated 4 times.
# [0,1,2,4,5,6,7] if it was rotated 7 times.
# Notice that rotating an array [a[0], a[1], a[2], ..., a[n-1]] 1 time results
# in the array [a[n-1], a[0], a[1], a[2], ..., a[n-2]].

# Given the sorted rotated array nums of unique elements, return the minimum
# element of this array.

# You must write an algorithm that runs in O(log n) time.

from typing import List


class Solution:
    def findMin(self, nums: List[int]) -> int:
        # We can still use binary search:
        #   If the left value is less than the pivot, then the minimum value
        #   is to the right.
        #   Otherwise, it's to the left.
        #   Note that in case of equality, we cannot use this method since:
        #     e.g. [2, 1, 2, 2, 2, 2] (1 rotation), and
        #          [2, 2, 2, 1, 2, 2] (3 rotations) are indistinguishable by
        #          the pivot comparison.
        #   But all elements are unique in our case.
        # Also due to uniqueness, if left value is less than right value, then
        # the number of rotations is some multiple of the length of nums.
        left = 0
        right = len(nums) - 1
        if nums[left] < nums[right] or len(nums) == 1:
            return nums[left]

        while left < right:
            pivot = (left + right) // 2
            if nums[left] < nums[pivot]:
                left = pivot
            else:
                right = pivot

        return nums[left + 1]
