# https://leetcode.com/problems/contains-duplicate/
# Given an integer array nums, return true if any value appears at least twice
# in the array, and return false if every element is distinct.

from typing import List


class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        elements = set()
        for num in nums:
            if num in elements:
                return True
            
            elements.add(num)
        return False
