# https://leetcode.com/problems/rectangle-area/
# Given the coordinates of two rectilinear rectangles in a 2D plane,
# return the total area covered by the two rectangles.
# The first rectangle is defined by its bottom-left corner (ax1, ay1) and its top-right corner (ax2, ay2).
# The second rectangle is defined by its bottom-left corner (bx1, by1) and its top-right corner (bx2, by2).

class Solution:
    def rectangularArea(self, x1: int, y1: int, x2: int, y2: int) -> int:
        delta_x = abs(x1 - x2)
        delta_y = abs(y1 - y2)
        return delta_x * delta_y

    def overlappingArea(self, ax1: int, ay1: int, ax2: int, ay2: int, bx1: int, by1: int, bx2: int, by2: int) -> int:
        if (ax1 >= bx2 or  # a right of b
            ax2 <= bx1 or  # a left of b
            ay1 >= by2 or  # a above b
            ay2 <= by1):  # a below b
            return 0

        inner_x1 = max(ax1, bx1)
        inner_y1 = max(ay1, by1)
        inner_x2 = min(ax2, bx2)
        inner_y2 = min(ay2, by2)
        return self.rectangularArea(inner_x1, inner_y1, inner_x2, inner_y2)

    def computeArea(self, ax1: int, ay1: int, ax2: int, ay2: int, bx1: int, by1: int, bx2: int, by2: int) -> int:
        a_area = self.rectangularArea(ax1, ay1, ax2, ay2)
        b_area = self.rectangularArea(bx1, by1, bx2, by2)
        overlapping_area = self.overlappingArea(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2)
        return (a_area + b_area) - overlapping_area
