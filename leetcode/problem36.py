# https://leetcode.com/problems/valid-sudoku/
# Determine if a 9 x 9 Sudoku board is valid. Only the filled cells need to be
# validated according to the following rules:

# Each row must contain the digits 1-9 without repetition.
# Each column must contain the digits 1-9 without repetition.
# Each of the nine 3 x 3 sub-boxes of the grid must contain the digits 1-9
# without repetition.

# Note:

# A Sudoku board (partially filled) could be valid but is not necessarily
# solvable.
# Only the filled cells need to be validated according to the mentioned rules.

from typing import List


class Solution:
    def isValidSudoku(self, board: List[List[str]]) -> bool:
        for row in self.getRows(board):
            if not self.isValidGroup(row):
                return False
        for col in self.getCols(board):
            if not self.isValidGroup(col):
                return False
        for box in self.getBoxes(board):
            if not self.isValidGroup(box):
                return False
        return True

    def isValidGroup(self, group: List[str]) -> bool:
        count = {num: 0 for num in range(1, 10)}
        for item in group:
            if item == ".":
                continue

            num = int(item)
            count[num] += 1
            if count[num] > 1:
                return False
        return True

    def getRows(self, board: List[List[str]]) -> List[str]:
        for row in board:
            yield row

    def getCols(self, board: List[List[str]]) -> List[str]:
        for j in range(0, 9):
            col = [board[i][j] for i in range(0, 9)]
            yield col

    def getBoxes(self, board: List[List[str]]) -> List[str]:
        for k in range(0, 9):
            i = 3 * (k // 3)
            j = 3 * (k % 3)
            box = [
                board[i][j], board[i][j + 1], board[i][j + 2],
                board[i + 1][j], board[i + 1][j + 1], board[i + 1][j + 2],
                board[i + 2][j], board[i + 2][j + 1], board[i + 2][j + 2],
            ]
            yield box
