# https://leetcode.com/problems/two-sum/
# Given an array of integers nums and an integer target, return two different
# indices such that their corresponding elements sum up to target.
# Assume that each input have exactly one solution.

from typing import List


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        num_index = {}

        for i, num in enumerate(nums):
            complement = target - num
            if complement in num_index:
                j = num_index[complement]
                if j < i:
                    return [j, i]
            
            num_index[num] = i
        
        return None
