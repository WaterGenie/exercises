# https://leetcode.com/problems/final-value-of-variable-after-performing-operations/
# There is a programming language with only four operations and one variable X:
# ++X and X++ increments the value of the variable X by 1.
# --X and X-- decrements the value of the variable X by 1.
# Initially, the value of X is 0.

# Given an array of strings operations containing a list of operations, return
# the final value of X after performing all the operations.

from typing import List


class Solution:
    def finalValueAfterOperations(self, operations: List[str]) -> int:
        value_so_far = 0
        increment_operations = ["++X", "X++"]
        decrement_operations = ["--X", "X--"]
        for operation in operations:
            if operation in increment_operations:
                value_so_far += 1
            elif operation in decrement_operations:
                value_so_far -= 1
        return value_so_far
