# https://leetcode.com/problems/container-with-most-water/
# You are given an integer array height of length n. There are n vertical lines
# drawn such that the two endpoints of the ith line are (i, 0) and
# (i, height[i]).

# Find two lines that together with the x-axis form a container, such that the
# container contains the most water.

# Return the maximum amount of water a container can store.

# Notice that you may not slant the container.

from typing import List


class Solution:
    def maxArea(self, heights: List[int]) -> int:
        area_so_far = 0

        # For a given width W and two lines of height h and H with h < H,
        # moving the H line inward will never produce a larger area since the
        # height will still be at most h, and the new width w is smaller than
        # W, so we can move the h line and potentially find a new line such
        # that the area is larger.
        left_head = 0
        right_head = len(heights) - 1
        while left_head < right_head:
            left_height = heights[left_head]
            right_height = heights[right_head]
            width = right_head - left_head
            height = min(left_height, right_height)
            area = width * height
            if area > area_so_far:
                area_so_far = area
            
            if left_height < right_height:
                left_head += 1
            else:
                right_head -= 1

        return area_so_far
