# https://leetcode.com/problems/length-of-last-word/
# Given a string s consisting of words and spaces, return the length of the last word in the string.

class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        words = s.strip().split(" ")
        return len(words[-1])
