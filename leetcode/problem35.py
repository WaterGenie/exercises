# https://leetcode.com/problems/search-insert-position/
# Given a sorted array of distinct integers and a target value, return the index if the target is found.
# If not, return the index where it would be if it were inserted in order.

from math import ceil
from typing import List


class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        if target <= nums[0]:
            return 0
        if target > nums[-1]:
            return len(nums)
        
        # target is in-between
        lower_bound = 0
        upper_bound = len(nums) - 1
        while lower_bound < upper_bound:
            index = ceil((lower_bound + upper_bound) / 2)
            print(f"lower: {lower_bound}, upper: {upper_bound}, index: {index}")
            num_at_index = nums[index]
            if target <= num_at_index:
                upper_bound = index
            else:
                lower_bound = index
            if index == upper_bound:
                lower_bound += 1
        
        return lower_bound


if __name__ == '__main__':
    s = Solution()
    print(s.searchInsert([1,5,5,5,6], 5))
