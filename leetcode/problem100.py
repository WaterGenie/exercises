# https://leetcode.com/problems/same-tree/
# Given the roots of two binary trees p and q, write a function to check if they are the same or not.
# Two binary trees are considered the same if they are structurally identical, and the nodes have the same value.

from typing import Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def isSameTree(self, p: Optional[TreeNode], q: Optional[TreeNode]) -> bool:
        if p is None and q is None:
            return True
        if p is None or q is None:
            return False
        # p and q are both non-empty

        # checking while doing any traversal method also works
        p_traversal = []
        p_stack = [p]
        q_traversal = []
        q_stack = [q]

        while len(p_stack) > 0:
            if len(q_stack) == 0:
                return False

            p_node = p_stack.pop()
            q_node = q_stack.pop()
            if p_node.val != q_node.val:
                return False
            p_traversal.append(p_node.val)
            q_traversal.append(q_node.val)

            if (p_node.right and not q_node.right) or (not p_node.right and q_node.right):
                return False
            if (p_node.left and not q_node.left) or (not p_node.left and q_node.left):
                return False

            if p_node.right:
                p_stack.append(p_node.right)
            if p_node.left:
                p_stack.append(p_node.left)
            if q_node.right:
                q_stack.append(q_node.right)
            if q_node.left:
                q_stack.append(q_node.left)
        
        return p_traversal == q_traversal
