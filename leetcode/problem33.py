# https://leetcode.com/problems/search-in-rotated-sorted-array/
# There is an integer array nums sorted in ascending order (with distinct
# values).

# Prior to being passed to your function, nums is possibly rotated at an
# unknown pivot index k (1 <= k < nums.length) such that the resulting array is
# [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]]
# (0-indexed). For example, [0,1,2,4,5,6,7] might be rotated at pivot index 3
# and become [4,5,6,7,0,1,2].

# Given the array nums after the possible rotation and an integer target,
# return the index of target if it is in nums, or -1 if it is not in nums.

# You must write an algorithm with O(log n) runtime complexity.

from typing import List


class Solution:
    def search(self, nums: List[int], target: int) -> int:
        # We can search for the minimum first in O(log n) time. This will allow
        # us to un-rotate the array and search for the target value normally.
        # At any moment, if we've come across the target value, then we can
        # simply return early.
        target_index = -1
        if len(nums) == 1:
            if nums[0] == target:
                target_index = 0
            return target_index

        # Find minimum so we can un-rotate.
        left = 0
        right = len(nums) - 1
        if nums[left] == target:
            return left
        if nums[right] == target:
            return right
        while left < right:
            pivot = (left + right) // 2
            if nums[pivot] == target:
                return pivot

            if nums[left] < nums[pivot]:
                left = pivot
            else:
                right = pivot
        
        # Instead of un-rotating, we also simply do binary search and work with
        # offset indices modulo len(nums)
        offset = left + 1
        length = len(nums)
        left = 0
        right = length - 1
        while left < right:
            pivot = (left + right) // 2
            shifted_pivot = (pivot + offset) % length
            if nums[shifted_pivot] == target:
                return shifted_pivot
            if left + 1 == right:
                break
            
            if nums[shifted_pivot] < target:
                left = pivot
            else:
                right = pivot

        return target_index
