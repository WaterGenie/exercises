# https://leetcode.com/problems/meeting-rooms/
# https://www.lintcode.com/problem/920/description
# Given an array of meeting time intervals consisting of start and end times
# [[s1,e1],[s2,e2],...] (si < ei), determine if a person could attend all
# meetings.

# Note: (0,8) and (8,10) do not conflict at 8

from typing import List


class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end


class Solution:
    """
    @param intervals: an array of meeting time intervals
    @return: if a person could attend all meetings
    """
    def can_attend_meetings(self, intervals: List[Interval]) -> bool:
        if len(intervals) <= 1:
            return True

        # Sort the intervals by their starting time. If the ending time is
        # greater than the next starting time, then we have a conflict.
        sorted_meetings = sorted(intervals, key=lambda meeting: meeting[0])
        for i, meeting in enumerate(sorted_meetings[:-1]):
            ending_time = meeting[1]
            next_starting_time = sorted_meetings[i + 1][0]
            if ending_time > next_starting_time:
                return False
        
        return True
