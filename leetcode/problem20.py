# https://leetcode.com/problems/valid-parentheses/
# Given a string s containing just the characters
# '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

# An input string is valid if:
#     Open brackets must be closed by the same type of brackets.
#     Open brackets must be closed in the correct order.
#     Every close bracket has a corresponding open bracket of the same type.

class Solution:
    def isOpenBracket(self, bracket: str) -> bool:
        return bracket in ['(', '{', '[']

    def isValidCloseBracketPop(self, bracket: str, stack: list[str]) -> bool:
        if len(stack) == 0:
            return False
        
        previous_bracket = stack[-1]
        validBracket = previous_bracket == '(' and bracket == ')'
        validCurlyBracket = previous_bracket == '{' and bracket == '}'
        validSquareBracket = previous_bracket == '[' and bracket == ']'

        isValid = validBracket or validCurlyBracket or validSquareBracket
        if isValid:
            del stack[-1]
        return isValid


    def isValid(self, s: str) -> bool:
        bracket_stack = []
        for bracket in s:
            if self.isOpenBracket(bracket):
                bracket_stack.append(bracket)
            else:
                is_valid_close_bracket = self.isValidCloseBracketPop(bracket, bracket_stack)
                if not is_valid_close_bracket:
                    return False

        return len(bracket_stack) == 0
