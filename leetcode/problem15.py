# https://leetcode.com/problems/3sum/
# Given an integer array nums, return all the triplets
# [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and
# nums[i] + nums[j] + nums[k] == 0.

# Notice that the solution set must not contain duplicate triplets.

from typing import List


class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        # Since the triplet must sum up to 0, either it's all 0s or there is a
        # mixture of both negative and positive numbers.
        positive_nums = []
        negative_nums = []
        positive_set = set()
        negative_set = set()
        zero_count = 0
        for num in nums:
            if num < 0:
                negative_nums.append(num)
                negative_set.add(num)
            elif num > 0:
                positive_nums.append(num)
                positive_set.add(num)
            else:
                zero_count += 1

        triplets = set()

        # Triplets with 1 or 3 zeros
        if zero_count >= 3:
            triplets.add((0, 0, 0))
        if zero_count >= 1:
            for negative in negative_nums:
                if abs(negative) in positive_nums:
                    triplets.add((negative, 0, abs(negative)))
        
        # Triplets with no zeros
        # For each negative pair, check if -1 times the sum is in the positive
        # set and vice versa
        for i, n1 in enumerate(negative_nums[:-1]):
            for n2 in negative_nums[i + 1:]:
                complement = abs(n1 + n2)
                if complement in positive_set:
                    if n1 < n2:
                        triplets.add((n1, n2, complement))
                    else:
                        triplets.add((n2, n1, complement))

        for i, p1 in enumerate(positive_nums[:-1]):
            for p2 in positive_nums[i + 1:]:
                complement = -(p1 + p2)
                if complement in negative_set:
                    if p1 < p2:
                        triplets.add((complement, p1, p2))
                    else:
                        triplets.add((complement, p2, p1))

        return list(triplets)
