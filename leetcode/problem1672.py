# https://leetcode.com/problems/richest-customer-wealth/
# You are given an m x n integer grid accounts where accounts[i][j] is the
# amount of money the i​​​​​​​​​​​th​​​​ customer has in the j​​​​​​​​​​​th​​​​ bank. Return the wealth
# that the richest customer has.

# A customer's wealth is the amount of money they have in all their bank
# accounts. The richest customer is the customer that has the maximum wealth.

from typing import List


class Solution:
    def maximumWealth(self, accounts: List[List[int]]) -> int:
        max_so_far = 0

        for customer_wealths in accounts:
            customer_wealth = 0
            for wealth in customer_wealths:
                customer_wealth += wealth
            if customer_wealth > max_so_far:
                max_so_far = customer_wealth

        return max_so_far
