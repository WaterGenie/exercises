# https://leetcode.com/problems/number-of-islands/
# Given an m x n 2D binary grid grid which represents a map of '1's (land) and
# '0's (water), return the number of islands.

# An island is surrounded by water and is formed by connecting adjacent lands
# horizontally or vertically. You may assume all four edges of the grid are all
# surrounded by water.

from typing import List
from typing import Set
from typing import Tuple


class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        # We can count the number of islands using graph traversal;
        # Starting from any unvisited node/cell, traverse until we've visited
        # every nodes reachable from the starting point.
        # Each such traversal forms 1 island.
        # Repeat until we've visited the whole grid.
        num_islands = 0

        visited_cells = set()
        for j, row in enumerate(grid):
            for i, state in enumerate(row):
                cell = (i, j)
                if cell in visited_cells:
                    continue

                if state == '1':
                    self.traverse(grid, cell, visited_cells)
                    num_islands += 1
        
        return num_islands

    def traverse(self, grid: List[List[str]], start: Tuple[int, int], visited_cells: Set[Tuple[int, int]]) -> None:
        stack = [start]
        while stack:
            cell = stack.pop()
            if cell in visited_cells:
                continue
            visited_cells.add(cell)

            x, y = cell
            state = grid[y][x]
            if state == '0':
                continue
            neighbors = self.getNeighbors(grid, cell)
            stack.extend(neighbors)

    def getNeighbors(self, grid: List[List[str]], cell: Tuple[int, int]) -> List[Tuple[int, int]]:
        width = len(grid[0])
        height = len(grid)
        neighbors = []

        for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            x = cell[0] + dx
            y = cell[1] + dy
            if 0 <= x and x < width and 0 <= y and y < height:
                neighbors.append((x, y))

        return neighbors
