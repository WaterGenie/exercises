# https://leetcode.com/problems/symmetric-tree/
# Given the root of a binary tree, check whether it is a mirror of itself
# (i.e., symmetric around its center).

from typing import Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def isMirrored(self, left: Optional[TreeNode], right: Optional[TreeNode]) -> bool:
        if left is None and right is None:
            # Both empty
            return True
        if left is None or right is None:
            # Exactly 1 empty
            return False
        
        # Both non-empty
        if left.val != right.val:
            return False
        return self.isMirrored(left.left, right.right) and self.isMirrored(left.right, right.left)

    def isSymmetricRecursive(self, node: Optional[TreeNode]) -> bool:
        return self.isMirrored(node.left, node.right)

    def isSymmetricIterative(self, node: Optional[TreeNode]) -> bool:
        if node.left is None and node.right is None:
            # Both empty
            return True
        if node.left is None or node.right is None:
            # Exactly 1 empty
            return False
        
        # Both non-empty
        left_head = node.left
        right_head = node.right
        left_stack = []
        right_stack = []
        # in-order traversal, but without the traversal list
        while left_head or left_stack:
            shape_mismatch = (left_head and not right_head) or (not left_head and right_head)
            value_mismatch = left_head and right_head and left_head.val != right_head.val
            if shape_mismatch or value_mismatch:
                return False
            
            if left_head:
                left_stack.append(left_head)
                left_head = left_head.left
            else:
                left_head = left_stack.pop()
                left_head = left_head.right
            
            # same traversal, but mirrored
            if right_head:
                right_stack.append(right_head)
                right_head = right_head.right
            else:
                right_head = right_stack.pop()
                right_head = right_head.left
        
        # Both sub-tree should finish traversal at the same time
        if right_head or right_stack:
            return False

        return True

    def isSymmetric(self, root: Optional[TreeNode]) -> bool:
        return self.isSymmetricIterative(root)
