# https://leetcode.com/problems/merge-two-sorted-lists/
# Given 2 sorted linked lists, merge into 1 sorted linked list.

from typing import Optional


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:
        if list1 is None:
            return list2
        if list2 is None:
            return list1
        
        # list1 and list2 are non-empty
        head1 = list1
        head2 = list2
        while head1 and head2:
            
