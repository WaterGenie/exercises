# https://leetcode.com/problems/how-many-numbers-are-smaller-than-the-current-number/
# Given the array nums, for each nums[i] find out how many numbers in the array
# are smaller than it. That is, for each nums[i] you have to count the number
# of valid j's such that j != i and nums[j] < nums[i].
# Return the answer in an array.

from typing import List


class Solution:
    def smallerNumbersThanCurrent(self, nums: List[int]) -> List[int]:
        num_smaller = [0] * len(nums)
        for i, num in enumerate(nums):
            for j, other in enumerate(nums):
                if i == j:
                    continue
                if other < num:
                    num_smaller[i] += 1
        return num_smaller
