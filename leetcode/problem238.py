# https://leetcode.com/problems/product-of-array-except-self/
# Given an integer array nums, return an array answer such that answer[i] is
# equal to the product of all the elements of nums except nums[i].

# The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit
# integer.

# You must write an algorithm that runs in O(n) time and without using the
# division operation.

# Follow up: Can you solve the problem in O(1) extra space complexity? (The
# output array does not count as extra space for space complexity analysis.)

from typing import List


class Solution:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        length = len(nums)
        products_except_self = [1] * length

        # Each element is the product of nums to the left and right of the
        # corresponding index

        # left head:
        for i in range(1, length):
            products_except_self[i] = products_except_self[i - 1] * nums[i - 1]
        
        # right head:
        right_head_product = nums[length - 1]
        for i in range(length - 2, 0, -1):
            products_except_self[i] *= right_head_product
            right_head_product *= nums[i]
        products_except_self[0] *= right_head_product

        return products_except_self
