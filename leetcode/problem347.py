# https://leetcode.com/problems/top-k-frequent-elements/
# Given an integer array nums and an integer k, return the k most frequent
# elements. You may return the answer in any order.

from typing import List


class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        num_frequency = self.getNumFrequency(nums)
        
        # sort by indexing based on frequency
        count_indexed_num_list = [None] * len(nums)
        for num, frequency in num_frequency.items():
            count_index = frequency - 1
            if count_indexed_num_list[count_index] is None:
                count_indexed_num_list[count_index] = [num]
            else:
                count_indexed_num_list[count_index].append(num)
        
        num_by_frequency = []
        for num_list in count_indexed_num_list:
            if num_list is not None:
                num_by_frequency.extend(num_list)
        
        return num_by_frequency[-k:]

    def getNumFrequency(self, nums: List[int]) -> dict[int, int]:
        num_frequency = {}
        for num in nums:
            if num in num_frequency:
                num_frequency[num] += 1
            else:
                num_frequency[num] = 1
        return num_frequency
