# https://leetcode.com/problems/is-subsequence/
# Given two strings s and t, return true if s is a subsequence of t, or false
# otherwise.

# A subsequence of a string is a new string that is formed from the original
# string by deleting some (can be none) of the characters without disturbing
# the relative positions of the remaining characters. (i.e., "ace" is a
# subsequence of "abcde" while "aec" is not).

# Follow up: Suppose there are lots of incoming s, say s1, s2, ..., sk where
# k >= 109, and you want to check one by one to see if t has its subsequence.
# In this scenario, how would you change your code?

class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        if s == "":
            return True

        main_head = 0
        sub_head = 0
        while main_head < len(t):
            if s[sub_head] == t[main_head]:
                sub_head += 1
                if sub_head == len(s):
                    return True
            main_head += 1
        return False
