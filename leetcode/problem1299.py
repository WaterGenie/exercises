# https://leetcode.com/problems/replace-elements-with-greatest-element-on-right-side/
# Given an array arr, replace every element in that array with the greatest
# element among the elements to its right, and replace the last element with -1.

# After doing so, return the array.

from typing import List


class Solution:
    def replaceElements(self, arr: List[int]) -> List[int]:
        # By looping in reverse and keeping track of the maximum so far,
        # we can do this in O(n)
        length = len(arr)
        new_array = [None] * length
        new_array[-1] = -1
        if length < 2:
            return new_array

        reversed_running_max = float('-inf')
        for i in range(length - 2, -1, -1):
            new_num = arr[i + 1]
            if new_num > reversed_running_max:
                reversed_running_max = new_num
            new_array[i] = reversed_running_max

        return new_array
