# https://leetcode.com/problems/kids-with-the-greatest-number-of-candies/
# There are n kids with candies. You are given an integer array candies, where
# each candies[i] represents the number of candies the ith kid has, and an
# integer extraCandies, denoting the number of extra candies that you have.

# Return a boolean array result of length n, where result[i] is true if, after
# giving the ith kid all the extraCandies, they will have the greatest number
# of candies among all the kids, or false otherwise.

# Note that multiple kids can have the greatest number of candies.

from typing import List


class Solution:
    def kidsWithCandies(self, candies: List[int], extraCandies: int) -> List[bool]:
        max_candies = max(candies)
        max_if_given_extra = [False] * len(candies)

        for i, kid_candies in enumerate(candies):
            if kid_candies + extraCandies >= max_candies:
                max_if_given_extra[i] = True

        return max_if_given_extra
