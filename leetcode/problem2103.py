# https://leetcode.com/problems/rings-and-rods/
# There are n rings and each ring is either red, green, or blue. The rings are
# distributed across ten rods labeled from 0 to 9.

# You are given a string rings of length 2n that describes the n rings that are
# placed onto the rods. Every two characters in rings forms a color-position
# pair that is used to describe each ring where:

# The first character of the ith pair denotes the ith ring's color
# ('R', 'G', 'B').
# The second character of the ith pair denotes the rod that the ith ring is
# placed on ('0' to '9').

# For example, "R3G2B1" describes n == 3 rings: a red ring placed onto the rod
# labeled 3, a green ring placed onto the rod labeled 2, and a blue ring placed
# onto the rod labeled 1.

# Return the number of rods that have all three colors of rings on them.


class Solution:
    def countPoints(self, rings: str) -> int:
        num_rods = len(rings) // 2
        rods_with_3_colors = set()
        rod_colors_coverage = {}

        for i in range(num_rods):
            pair = i * 2
            color = rings[pair]
            rod = rings[pair + 1]
            if rod in rods_with_3_colors:
                continue

            if rod not in rod_colors_coverage:
                rod_colors_coverage[rod] = set(color)
            else:
                rod_colors_coverage[rod].add(color)
            
            if len(rod_colors_coverage[rod]) == 3:
                rods_with_3_colors.add(rod)

        return len(rods_with_3_colors)
