# https://leetcode.com/problems/sort-the-people/
# You are given an array of strings names, and an array heights that consists
# of distinct positive integers. Both arrays are of length n.

# For each index i, names[i] and heights[i] denote the name and height of the
# ith person.

# Return names sorted in descending order by the people's heights.

from typing import List


class Solution:
    def sortPeople(self, names: List[str], heights: List[int]) -> List[str]:
        decorated = zip(names, heights)
        sort_decorated = sorted(decorated, key=lambda element: element[1], reverse=True)
        sort_undecorated = [element[0] for element in sort_decorated]
        return sort_undecorated
