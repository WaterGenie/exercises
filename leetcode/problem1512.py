# https://leetcode.com/problems/number-of-good-pairs/
# Given an array of integers nums, return the number of good pairs.
# A pair (i, j) is called good if nums[i] == nums[j] and i < j.

from typing import List


class Solution:
    def numIdenticalPairs(self, nums: List[int]) -> int:
        num_pairs = 0
        for i, num in enumerate(nums):
            for other in nums[i + 1:]:
                if num == other:
                    num_pairs += 1
        return num_pairs
