# https://leetcode.com/problems/sqrtx/
# Given a non-negative integer x, return the square root of x rounded down to the nearest integer.
# The returned integer should be non-negative as well.
# You must not use any built-in exponent function or operator.

class Solution:
    def mySqrt(self, x: int) -> int:
        if x < 1:
            return 0
        if x == 1:
            return 1

        lower_bound = 1
        upper_bound = x
        candidate = (1 + x) // 2
        while lower_bound < upper_bound:
            candidate_squared = candidate * candidate
            if candidate_squared == x:
                return candidate
            if candidate_squared < x:
                if (candidate + 1) * (candidate + 1) > x:
                    return candidate
                else:
                    lower_bound = candidate
            else:
                upper_bound = candidate
            candidate = (lower_bound + upper_bound) // 2

        return candidate
