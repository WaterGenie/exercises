# https://leetcode.com/problems/longest-consecutive-sequence/
# Given an unsorted array of integers nums, return the length of the longest
# consecutive elements sequence.

# You must write an algorithm that runs in O(n) time.

from typing import List


class Solution:
    def longestConsecutive(self, nums: List[int]) -> int:
        unique_nums = set(nums)
        longest_so_far = 0

        for num in nums:
            is_start_of_sequence = num - 1 not in unique_nums
            if is_start_of_sequence:
                head = num + 1
                while head in unique_nums:
                    head += 1

                length = head - num
                if length > longest_so_far:
                    longest_so_far = length

        return longest_so_far
