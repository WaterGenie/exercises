# https://leetcode.com/problems/rearrange-array-elements-by-sign/
# You are given a 0-indexed integer array nums of even length consisting of an
# equal number of positive and negative integers.

# You should rearrange the elements of nums such that the modified array
# follows the given conditions:

# Every consecutive pair of integers have opposite signs.
# For all integers with the same sign, the order in which they were present in
# nums is preserved.
# The rearranged array begins with a positive integer.

# Return the modified array after rearranging the elements to satisfy the
# aforementioned conditions.

from typing import List


class Solution:
    def rearrangeArray(self, nums: List[int]) -> List[int]:
        positive_array = []
        negative_array = []
        for num in nums:
            if num > 0:
                positive_array.append(num)
            else:
                negative_array.append(num)

        arranged_array = []
        for i, pos in enumerate(positive_array):
            neg = negative_array[i]
            arranged_array.append(pos)
            arranged_array.append(neg)

        return arranged_array
