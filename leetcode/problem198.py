# https://leetcode.com/problems/house-robber/
# You are a professional robber planning to rob houses along a street. Each
# house has a certain amount of money stashed, the only constraint stopping you
# from robbing each of them is that adjacent houses have security systems
# connected and it will automatically contact the police if two adjacent houses
# were broken into on the same night.

# Given an integer array nums representing the amount of money of each house,
# return the maximum amount of money you can rob tonight without alerting the
# police.

from typing import List


class Solution:
    def rob(self, nums: List[int]) -> int:
        # Suppose we have the optimal values from all the previous houses,
        # then the optimal value of the current house i can be determined by
        # comparing:
        #   1. Rob i: gets money[i], skip money[i-1], gets value[i-2]
        #   2. Don't: gets value[i-1]
        if len(nums) == 0:
            return 0

        value_prev_prev = 0
        value_prev = 0
        for num in nums:
            rob_value = num + value_prev_prev
            skip_value = value_prev

            if rob_value > skip_value:
                value_prev = rob_value
            else:
                value_prev = skip_value
            value_prev_prev = skip_value
        
        # By the end of the list, value_prev holds the optimal value
        return value_prev
