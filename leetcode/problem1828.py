# https://leetcode.com/problems/queries-on-number-of-points-inside-a-circle/
# You are given an array points where points[i] = [xi, yi] is the coordinates
# of the ith point on a 2D plane. Multiple points can have the same coordinates.

# You are also given an array queries where queries[j] = [xj, yj, rj] describes
# a circle centered at (xj, yj) with a radius of rj.

# For each query queries[j], compute the number of points inside the jth circle.
# Points on the border of the circle are considered inside.

# Return an array answer, where answer[j] is the answer to the jth query.

from typing import List
from math import sqrt


class Solution:
    def countPoints(self, points: List[List[int]], queries: List[List[int]]) -> List[int]:
        num_points_in_circle = [0] * len(queries)

        for i, circle in enumerate(queries):
            for point in points:
                if self.isPointInCircle(point, circle):
                    num_points_in_circle[i] += 1

        return num_points_in_circle

    def isPointInCircle(self, point: List[int], circle: List[int]) -> bool:
        cx, cy, r = circle
        x, y = point
        return self.distanceBetween(x, y, cx, cy) <= r

    def distanceBetween(self, x1: int, y1: int, x2: int, y2: int) -> float:
        return sqrt((x1 - x2)**2 + (y1 - y2)**2)
