# https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/
# Given an array nums of n integers where nums[i] is in the range [1, n],
# return an array of all the integers in the range [1, n] that do not appear in
# nums.

from typing import List


class Solution:
    def findDisappearedNumbers(self, nums: List[int]) -> List[int]:
        # Sorting approach then 1 traversal gives O(n log n)
        # But traversal + hash set gives O(n)
        max_num = len(nums)
        remaining_nums = set(range(1, max_num + 1))

        for num in nums:
            if num in remaining_nums:
                remaining_nums.remove(num)

        return list(remaining_nums)
