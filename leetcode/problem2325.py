# https://leetcode.com/problems/decode-the-message/
# You are given the strings key and message, which represent a cipher key and a
# secret message, respectively. The steps to decode message are as follows:

# Use the first appearance of all 26 lowercase English letters in key as the
# order of the substitution table.
# Align the substitution table with the regular English alphabet.
# Each letter in message is then substituted using the table.
# Spaces ' ' are transformed to themselves.

# For example, given key = "happy boy" (actual key would have at least one
# instance of each letter in the alphabet), we have the partial substitution
# table of
# ('h' -> 'a', 'a' -> 'b', 'p' -> 'c', 'y' -> 'd', 'b' -> 'e', 'o' -> 'f').

# Return the decoded message.

class Solution:
    def decodeMessage(self, key: str, message: str) -> str:
        substitution = {}
        index = 97
        for char in key:
            if char == ' ':
                continue
            if char not in substitution:
                substitution[char] = chr(index)
                index += 1

        decoded_message_list = []
        for char in message:
            decoded_char = ' '
            if char != ' ':
                decoded_char = substitution[char]
            decoded_message_list.append(decoded_char)
        decoded_message = ''.join(decoded_message_list)

        return decoded_message
