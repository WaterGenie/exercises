# https://leetcode.com/problems/majority-element/
# Given an array nums of size n, return the majority element.
# The majority element is the element that appears more than ⌊n / 2⌋ times.
# Assume majority element exists.

from typing import List


class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        num_count = {}
        current_max = None
        max_count_so_far = float('-inf')

        for num in nums:
            if num not in num_count:
                num_count[num] = 1
            else:
                num_count[num] += 1
            if num_count[num] > max_count_so_far:
                max_count_so_far = num_count[num]
                current_max = num

        return current_max
