# https://leetcode.com/problems/longest-common-prefix/
# Given a list of string, find the longest common prefix.
# Return an empty string if there is no common prefix.

from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        first_word = strs[0]
        longest_so_far = len(first_word)
        common_prefix = list(first_word)

        for string in strs[1:]:
            for i, char in enumerate(string):
                if i >= longest_so_far:
                    break

                # i < longest_so_far
                if char == common_prefix[i]:
                    break
                else:
                    longest_so_far = i
                    common_prefix = common_prefix[:i]
        

    
        return "".join(common_prefix)


if __name__ == '__main__':
    s = Solution()

    strs = ["ab", "a"]
    # strs = ["dog","racecar","car"]
    print(s.longestCommonPrefix(strs))
