# https://leetcode.com/problems/binary-tree-level-order-traversal/
# Given the root of a binary tree, return the level order traversal of its
# nodes' values. (i.e., from left to right, level by level).

from typing import List
from typing import Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def levelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:
        if root is None:
            return []

        # Since we want to group each level into its own list, we proceed by
        # levels instead of by nodes.
        traversal = []
        
        # For each level, append the list of values into the result
        # Update level to be the leaves of the current level
        level = [root]
        while level:
            next_level = []
            level_values = []
            for node in level:
                if node is None:
                    continue
                level_values.append(node.val)
                if node.left:
                    next_level.append(node.left)
                if node.right:
                    next_level.append(node.right)
            traversal.append(level_values)
            level = next_level

        return traversal
