# https://leetcode.com/problems/goal-parser-interpretation/
# You own a Goal Parser that can interpret a string command. The command
# consists of an alphabet of "G", "()" and/or "(al)" in some order. The Goal
# Parser will interpret "G" as the string "G", "()" as the string "o", and
# "(al)" as the string "al". The interpreted strings are then concatenated in
# the original order.

# Given the string command, return the Goal Parser's interpretation of command.

import re


class Solution:
    def interpret(self, command: str) -> str:
        parsed_command = re.sub(r"\(al\)", "al", command)
        parsed_command = re.sub(r"\(\)", "o", parsed_command)
        parsed_command = re.sub(r"G", "G", parsed_command)
        return parsed_command
