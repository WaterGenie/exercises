# https://leetcode.com/problems/guess-number-higher-or-lower/
# Given an integer k in [1,n], guess(num: int) -> int returns:
#   -1 if num > k
#   1 if num < k
#   0 if num == k
# Find k.

class Solution:
    def guessNumber(self, n: int) -> int:
        if guess(1) == 0:
            return 1
        if guess(n) == 0:
            return n

        lower_bound = 1
        upper_bound = n
        candidate = None
        while lower_bound < upper_bound:
            candidate = (lower_bound + upper_bound) // 2
            result = guess(candidate)
            if result == -1:
                upper_bound = candidate
            elif result == 1:
                lower_bound = candidate
            else:
                break

        return candidate
