# https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
# You are given an array prices where prices[i] is the price of a given stock
# on the ith day.

# You want to maximize your profit by choosing a single day to buy one stock
# and choosing a different day in the future to sell that stock.

# Return the maximum profit you can achieve from this transaction. If you
# cannot achieve any profit, return 0.

from typing import List


class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        profit_so_far = 0

        # For any given buy/sell head pairs, if the price at the sell_head is
        # less than the price at the buy_head, then it is always more
        # profitable to buy from the sell_head day instead.
        # So we can just update the buy_head forward and continue, otherwise,
        # we check for the profit.
        buy_head = 0
        for sell_head in range(len(prices)):
            buy_price = prices[buy_head]
            sell_price = prices[sell_head]
            if sell_price < buy_price:
                buy_head = sell_head
            else:
                profit = sell_price - buy_price
                if profit > profit_so_far:
                    profit_so_far = profit

        return profit_so_far
