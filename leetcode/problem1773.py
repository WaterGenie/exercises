# https://leetcode.com/problems/count-items-matching-a-rule/
# You are given an array items, where each items[i] = [typei, colori, namei]
# describes the type, color, and name of the ith item. You are also given a
# rule represented by two strings, ruleKey and ruleValue.

# The ith item is said to match the rule if one of the following is true:
# ruleKey == "type" and ruleValue == typei.
# ruleKey == "color" and ruleValue == colori.
# ruleKey == "name" and ruleValue == namei.

# Return the number of items that match the given rule.

from typing import List


class Solution:
    def countMatches(self, items: List[List[str]], ruleKey: str, ruleValue: str) -> int:
        num_matches = 0

        key_to_index = {
            'type': 0,
            'color': 1,
            'name': 2
        }
        rule_index = key_to_index[ruleKey]

        for item in items:
            item_value = item[rule_index]
            if item_value == ruleValue:
                num_matches += 1

        return num_matches
