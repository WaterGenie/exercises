# https://leetcode.com/problems/maximum-number-of-balloons/
# Given a string text, you want to use the characters of text to form as many
# instances of the word "balloon" as possible.

# You can use each character in text at most once. Return the maximum number of
# instances that can be formed.

class Solution:
    def maxNumberOfBalloons(self, text: str) -> int:
        char_count = {char: 0 for char in "balon"}
        for char in text:
            if char in char_count:
                char_count[char] += 1
        
        count_divisor = {
            'b': 1,
            'a': 1,
            'l': 2,
            'o': 2,
            'n': 1,
        }
        min_count = float('inf')
        for char, count in char_count.items():
            normalized_count = count // count_divisor[char]
            if normalized_count < min_count:
                min_count = normalized_count
        return min_count
