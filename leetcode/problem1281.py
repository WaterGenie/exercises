# https://leetcode.com/problems/subtract-the-product-and-sum-of-digits-of-an-integer/
# Given an integer number n, return the difference between the product of its
# digits and the sum of its digits.

class Solution:
    def subtractProductAndSum(self, n: int) -> int:
        digits_product = 1
        digits_sum = 0

        i = n
        while i > 0:
            digit = i % 10

            digits_product *= digit
            digits_sum += digit

            i //= 10

        return digits_product - digits_sum
