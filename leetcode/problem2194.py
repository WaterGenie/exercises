# https://leetcode.com/problems/cells-in-a-range-on-an-excel-sheet/
# A cell (r, c) of an excel sheet is represented as a string "<col><row>" where:

# <col> denotes the column number c of the cell. It is represented by
# alphabetical letters.
# For example, the 1st column is denoted by 'A', the 2nd by 'B', the 3rd by
# 'C', and so on.

# <row> is the row number r of the cell. The rth row is represented by the
# integer r.

# You are given a string s in the format "<col1><row1>:<col2><row2>", where
# <col1> represents the column c1, <row1> represents the row r1, <col2>
# represents the column c2, and <row2> represents the row r2, such that
# r1 <= r2 and c1 <= c2.

# Return the list of cells (x, y) such that r1 <= x <= r2 and c1 <= y <= c2.
# The cells should be represented as strings in the format mentioned above and
# be sorted in non-decreasing order first by columns and then by rows.

from typing import List


class Solution:
    def cellsInRange(self, s: str) -> List[str]:
        top_left_cell, bottom_right_cell = s.split(":")
        start_col, start_row = top_left_cell
        end_col, end_row = bottom_right_cell
        cols_in_range = self.getRange(start_col, end_col)
        rows_in_range = self.getRange(start_row, end_row)

        cells_in_range = []
        for col in cols_in_range:
            for row in rows_in_range:
                cell = col + row
                cells_in_range.append(cell)
        
        return cells_in_range

    def getRange(self, start: str, end: str) -> List[str]:
        row_or_col_range = [start]

        current = start
        while current != end:
            current = chr(ord(current) + 1)
            row_or_col_range.append(current)

        return row_or_col_range
