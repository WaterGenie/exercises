# https://leetcode.com/problems/single-number/
# Given a non-empty array of integers nums, every element appears twice except for one.
# Find that single one.
# You must implement a solution with a linear runtime complexity and use only constant extra space.

from typing import List


class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        # naive solution uses O(n) space
        # a XOR a = 0 for all a
        # So XOR-ing everything will cancel out everything except the
        # non-repeated number.
        xor_so_far = 0
        for num in nums:
            xor_so_far ^= num
        
        return xor_so_far
