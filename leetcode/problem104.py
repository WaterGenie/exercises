# https://leetcode.com/problems/maximum-depth-of-binary-tree/
# Given the root of a binary tree, return its maximum depth.

# A binary tree's maximum depth is the number of nodes along the longest path
# from the root node down to the farthest leaf node.

from typing import Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def maxDepth(self, root: Optional[TreeNode]) -> int:
        if root is None:
            return 0
        
        max_depth = 1

        head = root
        stack = [(head, 1)]
        while stack:
            head, depth = stack.pop()
            depth += 1
            if head.left:
                stack.append((head.left, depth))
            if head.right:
                stack.append((head.right, depth))
            if (head.left or head.right) and depth > max_depth:
                max_depth = depth

        return max_depth
