# https://leetcode.com/problems/palindrome-number/
# Given an integer x, return true if x is a palindrome, and false otherwise.

class Solution:
    def isPalindrome(self, x: int) -> bool:
        if x == 0:
            return True
        if x < 0:
            # Test cases are equivalent to doing a string reverse so all
            # negatives are not palindrome...
            return False

        i = x
        reverse_x = 0
        while i > 0:
            digit = i % 10
            reverse_x *= 10
            reverse_x += digit
            i //= 10
        
        return x == reverse_x
