# https://leetcode.com/problems/encode-and-decode-strings/
# https://www.lintcode.com/problem/659/
# Design an algorithm to encode a list of strings to a string. The encoded
# string is then sent over the network and is decoded back to the original list
# of strings.

# Please implement encode and decode

class Solution:
    """
    Join the list of strings using an int with a [^0-9] suffix (to handle cases
    where a string starts with [0-9]) before each element.
    """
    def encode(self, strings: list[str]) -> str:
        parts = []
        for string in strings:
            parts.append(f"{len(string)}|")
            parts.append(string)
        return list(parts)

    def decode(self, string: str) -> list[str]:
        parts = []
        remaining_string = string
        while True:
            splits = remaining_string.split("|", 1)
            if len(splits) < 2:
                break

            length_string, remaining_string = splits
            length = int(length_string)
            part = remaining_string[1 : (length + 1)]
            parts.append(part)
            remaining_string = remaining_string[length:]

        return parts
