# https://leetcode.com/problems/partitioning-into-minimum-number-of-deci-binary-numbers/
# A decimal number is called deci-binary if each of its digits is either 0 or 1
# without any leading zeros. For example, 101 and 1100 are deci-binary, while
# 112 and 3001 are not.
# Given a string n that represents a positive decimal integer, return the
# minimum number of positive deci-binary numbers needed so that they sum up to
# n.

class Solution:
    def minPartitions(self, n: str) -> int:
        # Since any given digit in a deci-binary number is at most 1,
        # we need at least as many deci-binary number as the largest digit.
        largest_digit_so_far = 0
        for digit_char in n:
            digit = int(digit_char)
            if digit == 9:
                return digit
            if digit > largest_digit_so_far:
                largest_digit_so_far = digit
        return largest_digit_so_far
