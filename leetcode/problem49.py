# https://leetcode.com/problems/group-anagrams/
# Given an array of strings strs, group the anagrams together. You can return
# the answer in any order.

# An Anagram is a word or phrase formed by rearranging the letters of a
# different word or phrase, typically using all the original letters exactly
# once.

from typing import List


class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        anagram_strings = {}

        for string in strs:
            char_frequency = self.getCharFrequency(string)
            anagram = repr(char_frequency)
            if anagram in anagram_strings:
                anagram_strings[anagram].append(string)
            else:
                anagram_strings[anagram] = [string]
        
        return list(anagram_strings.values())

    def getCharFrequency(self, string: str) -> dict[str, int]:
        char_frequency = [0] * 26
        for char in string:
            index = ord(char) - ord('a')
            char_frequency[index] += 1
        return char_frequency
