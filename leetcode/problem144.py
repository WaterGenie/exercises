# https://leetcode.com/problems/binary-tree-preorder-traversal/
# Given the root of a binary tree, return the pre-order traversal of its nodes' values.

from typing import List
from typing import Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def preorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        if root is None:
            return []

        traversal = []
        stack = [root]

        while len(stack) > 0:
            node = stack.pop()
            traversal.append(node.val)
            if node.right:
                stack.append(node.right)
            if node.left:
                stack.append(node.left)
        
        return traversal
