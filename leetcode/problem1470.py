# https://leetcode.com/problems/shuffle-the-array/
# Given the array nums consisting of 2n elements in the form
# [x1,x2,...,xn,y1,y2,...,yn].
# Return the array in the form [x1,y1,x2,y2,...,xn,yn].

from typing import List


class Solution:
    def shuffle(self, nums: List[int], n: int) -> List[int]:
        shuffled_array = [None] * len(nums)
        for i in range(n):
            write_index = 2 * i
            shuffled_array[write_index] = nums[i]
            shuffled_array[write_index + 1] = nums[i + n]
        return shuffled_array
